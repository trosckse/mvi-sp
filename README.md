# mvi-sp

# Identifying Proteins Critical to Learning in a Mouse Model of Down Syndrome

Background

Down syndrome (DS) is a chromosomal abnormality associated with intellectual disability and affecting approximately 1 in 1000 live births worldwide. The overexpression of genes encoded by the extra copy of a normal chromosome in DS is believed to be sufficient to perturb normal pathways and normal responses to stimulation, causing learning and memory deficits. Measurements were made using reverse phase protein arrays (RPPA), a high throughput technique in which protein samples from individual mice are robotically spotted onto nitrocellulose-coated microscope slides. 

Project aim

The aim of this project is to create an efficient nerual network to identify the classes of the mices(basing on genotype, treatment type, behavior). 

Input Data

The dataset(https://archive.ics.uci.edu/ml/datasets/Mice+Protein+Expression) consists of the expression levels of 77 proteins/protein modifications that produced detectable signals in the nuclear fraction of cortex. There are 38 control mice and 34 trisomic mice (Down syndrome), for a total of 72 mice. In the experiments, 15 measurements were registered of each protein per sample/mouse. Therefore, for control mice, there are 38x15, or 570 measurements, and for trisomic mice, there are 34x15, or 510 measurements. The dataset contains a total of 1080 measurements per protein. Each measurement can be considered as an independent sample/mouse.

